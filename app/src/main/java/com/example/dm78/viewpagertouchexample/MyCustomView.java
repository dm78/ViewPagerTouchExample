package com.example.dm78.viewpagertouchexample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyCustomView extends FrameLayout {

    public static final String TAG = MyCustomView.class.getSimpleName();

    private PanelData mPanelData;
    private LinearLayout mFrontPanel;
    private float mFrontPanelInitialX = Float.NaN;
    private float mDownX = Float.NaN;
    private boolean mFrontPanelVisible = false;
    private int mOffScreenThreshold;

    public MyCustomView(Context context, PanelData holder) {
        super(context);
        mPanelData = holder;
        init();
    }

    public MyCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        View view = View.inflate(getContext(), R.layout.my_custom_view, this);
        mFrontPanel = (LinearLayout) view.findViewById(R.id.front_panel);
        TextView mFrontTextView = (TextView) findViewById(R.id.front_textView);
        TextView mRearTextView = (TextView) findViewById(R.id.rear_textView);

        mFrontTextView.setText(mPanelData.frontText);
        mRearTextView.setText(mPanelData.rearText);

        if (mPanelData.showFrontPanel) {
            mFrontPanel.setVisibility(View.VISIBLE);
            mFrontPanelVisible = true;
        }

        mOffScreenThreshold = -(getContext().getResources().getDisplayMetrics().widthPixels / 3);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        mFrontPanelInitialX = mFrontPanel.getX();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mPanelData.showFrontPanel && mFrontPanelVisible) {
            Log.d(TAG, "I WANT THIS EVENT  " + ev);
            getParent().requestDisallowInterceptTouchEvent(true);
            return true;
        }
        Log.d(TAG, "i don't care about this event  " + ev);
        return false;
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        Log.d(TAG, event.toString());
        boolean result = false;
        int xDir = 0;
        float newX = 0;

        if (mDownX != Float.NaN) {
            xDir = (int) (event.getRawX() - mDownX);
            newX = event.getRawX() - mDownX;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mDownX = event.getRawX();
                result = true;
                break;
            case MotionEvent.ACTION_MOVE:
                if (mFrontPanel.getX() >= 0 && xDir > 0) {
                    Log.d(TAG, "move cancelling");
                    getParent().requestDisallowInterceptTouchEvent(false);
                    break;
                } else {
                    if (newX < 0 && newX > mOffScreenThreshold) {
                        // animate to new pos
                        Log.d(TAG, "move animate to new pos " + newX);
                        mFrontPanel.animate().x(newX).setDuration(0).start();
                        mFrontPanelVisible = true;
                    } else if (newX < 0 && newX <= mOffScreenThreshold) {
                        // animate offscreen
                        Log.d(TAG, "move animate offscreen");
                        mFrontPanel.animate().x(-mFrontPanel.getWidth()).setDuration(200).start();
                        mFrontPanelVisible = false;
                        getParent().requestDisallowInterceptTouchEvent(false);
                    } else if (newX > 0) {
                        // animate to original pos
                        Log.d(TAG, "move animate to original pos");
                        mFrontPanel.animate().x(mFrontPanelInitialX).setDuration(0).start();
                        mFrontPanelVisible = true;
                    } else {
                        Log.d(TAG, "move idk what to do X = " + mFrontPanel.getX() + ", xDir = " + xDir + ", newX = " + newX);
                    }
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                Log.d(TAG, "Action cancelled");
                getParent().requestDisallowInterceptTouchEvent(false);
                mDownX = Float.NaN;
                break;
                // fall through
            case MotionEvent.ACTION_UP:
                if (!mFrontPanelVisible) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                }

                if (mFrontPanelVisible && xDir != 0) {
                    if (mFrontPanel.getX() >= 0 && xDir > 0) {
                        Log.d(TAG, "up ignore");
                        getParent().requestDisallowInterceptTouchEvent(false);
                    }
                    else if (mFrontPanel.getX() <= mOffScreenThreshold) {
                        Log.d(TAG, "up animate offscreen");
                        mFrontPanel.animate().x(-mFrontPanel.getWidth()).setDuration(200).start();
                        mFrontPanelVisible = false;
                        getParent().requestDisallowInterceptTouchEvent(false);
                    } else if (mFrontPanel.getX() > mOffScreenThreshold) {
                        Log.d(TAG, "up animate to original pos");
                        mFrontPanel.animate().x(mFrontPanelInitialX).setDuration(200).start();
                        mFrontPanelVisible = true;
                    }
                }
                mDownX = Float.NaN;
                break;
            default:
                result = super.onTouchEvent(event);
        }

        return result;
    }

    public static class PanelData {
        public String rearText;
        public String frontText;
        public boolean showFrontPanel;
    }
}
