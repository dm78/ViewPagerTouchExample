package com.example.dm78.viewpagertouchexample;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class MyPagerAdapter extends PagerAdapter {

    public static final String TAG = MyPagerAdapter.class.getSimpleName();
    
    private static final ArrayList<MyCustomView.PanelData> PANEL_DATA_LIST = new ArrayList<>();
    
    static {
        MyCustomView.PanelData panelData = new MyCustomView.PanelData();
        panelData.showFrontPanel = false;
        panelData.frontText = null;
        panelData.rearText = "First page without front panel";
        PANEL_DATA_LIST.add(panelData);

        panelData = new MyCustomView.PanelData();
        panelData.showFrontPanel = true;
        panelData.frontText = "Second page with front panel";
        panelData.rearText = "Second page rear panel";
        PANEL_DATA_LIST.add(panelData);

        panelData = new MyCustomView.PanelData();
        panelData.showFrontPanel = false;
        panelData.frontText = null;
        panelData.rearText = "Third page without front panel";
        PANEL_DATA_LIST.add(panelData);

        panelData = new MyCustomView.PanelData();
        panelData.showFrontPanel = true;
        panelData.frontText = "Fourth page with front panel";
        panelData.rearText = "Fourth page rear panel";
        PANEL_DATA_LIST.add(panelData);

        panelData = new MyCustomView.PanelData();
        panelData.showFrontPanel = false;
        panelData.frontText = null;
        panelData.rearText = "Fifth page without front panel";
        PANEL_DATA_LIST.add(panelData);
    }
     
    @Override
    public int getCount() {
        return PANEL_DATA_LIST.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view.equals(o);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        MyCustomView view = new MyCustomView(container.getContext(), PANEL_DATA_LIST.get(position));
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
